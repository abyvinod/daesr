% Implementation of the position estimate of the spherical robot 
% (Equation 10 in the paper doi: 10.1109/TMECH.2015.2404343)
%
% Implementation by Abraham P Vinod (aby.vinod@gmail.com)
%
% Arguments
% =========
%
% xpos   - Current x-coordinate
% ypos   - Current y-coordinate
% R      - Attitude estimate
% gy     - Rate gyro readout
% rad    - Radius of the spherical robot
% dt     - Sampling time interval
%
% Outputs
% =======
%
% xposnew- Updated x-coordinate
% yposnew- Updated y-coordinate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [xposnew,yposnew]=getpos(xpos,ypos,R,gy,rad,dt)
    r1 = R(1,:).';
    r2 = R(2,:).';
    
    xposnew = xpos + rad*r2.'*gy*dt;
    yposnew = ypos - rad*r1.'*gy*dt;
    
end