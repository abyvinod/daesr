% Implementation of the passive nonlinear complimentary filter without bias
% correction (Equation 10 in the paper doi: 10.1109/TAC.2008.923738) 
%
% Implementation by Abraham P Vinod (aby.vinod@gmail.com)
%
% Arguments
% =========
%
% Rcap   - Current attitude estimate of the filter
% wcap   - Body angular velocity written in the skew-symmetric matrix form
% dt     - Sampling time interval
% kmaho  - Tuning parameter of the filter
% R_meas - Measured estimate of the attitude
%
% Outputs
% =======
%
% Rcap   - Updated attitude estimate
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [Rcap]=noncomp(Rcap,wcap,dt,kmaho,R_meas)
    rtil = Rcap.'*R_meas;
    omcap = 1/2*(rtil-rtil.');
    Rcap = Rcap*expm((wcap+kmaho*omcap)*dt);
end
