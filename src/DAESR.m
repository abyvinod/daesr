% DAESR algorithm as provided in Algorithm 1 presented in A Deterministic 
% Attitude Estimation Using a Single Vector Information and Rate Gyros 
% doi: 10.1109/TMECH.2015.2404343
%
% Implementation by Abraham P Vinod (aby.vinod@gmail.com)
%
% Arguments
% =========
%
% acc       - Normalized vector pointing to the body frame resolution of downward
%               vertical (generalized to -e_3)
% gyro      - Rate gyro reading
% h         - Sampling time
% alpha     - Current value of the angle of rotation about z-axis (set to
%               zero in the first call)
% accprev   - Previous acc
% Rb        - Rotation matrix linking IMU frame to body frame - Rb_{Body}^{IMU}
% R_C       - Rotation matrix that ensures continuity while transition from
%               S_U to S_L and vice versa. It serves as the R_I.
% alpha_eps - Threshold to cutoff noise in the integration
%
% Outputs
% =======
%
% R         - Attitude estimate using DAESR algorithm
% alpha     - Updated value of the angle of rotation about z-axis (Used for 
%               future calls)
% R_C       - Updated rotation matrix that ensures continuity while transition 
%               from S_U to S_L and vice versa. (Used for future calls)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [R,alpha,R_C]=DAESR(acc,gyro,h,alpha,accprev,Rb,R_C,alpha_eps)
    
    %% Initializing variables
    theta_t=2*pi/3;                      % Definition of theta_t and cosine
    costhresh=cos(theta_t);
    e3=[0;0;1];                          % Definition of e_3
    flag=0;                              % Tracking transitions
    
    %% Resolve the measurements taken in the IMU frame to the body frame
    
    gyro=Rb*gyro;                        % Body angular velocity
    acc=Rb*(acc./norm(acc));             % acc is the normalized resolution 
                                         % of -e_3 in the body frame.
    accprev=Rb*(accprev./norm(accprev)); % acc_prev is the normalized resolution 
                                         % of -e_3 in the previous instant 
                                         % in the body frame.
    ax=acc(1);
    ay=acc(2);
    az=acc(3);
    costheta=dot(acc,-e3);                                     
    costhetap=dot(accprev,-e3);                                     

    %% DAESR algorithm
    
    %% Lower Hemisphere
    if (costheta>=costhresh)
        % Use the modified Rodriguez's formula to estimate Rv.
        % If costhetap <= costhresh, then a transition occured. Hence, 
        % compute R_E using Proposition III.1, alphadiff using Equation 6 
        % and alpha by integration to obtain R. Conclude, by updating R_C 
        % using R,Rv and setting alpha=0. 
        % Else, update alpha using alphadiff (Eqn 6) and estimate R using 
        % Rv,alpha and R_C.
        
        n=cross(acc,-e3);                                     
        K=[0,-n(3),n(2);
          n(3),0,-n(1);
          -n(2),n(1),0];   
        Rv=eye(3,3)+K+(K*K)/(1+costheta);

        if(costhetap<=costhresh)
            ay2az2=sqrt(ay^2+az^2);
            Rv_alt=[ay2az2,-(ax*ay)/ay2az2,-(ax*az)/ay2az2;
                    0,-az/ay2az2,ay/ay2az2;
                    -ax,-ay,-az];               % Proposition III.1
            
            inertgyro=Rv_alt*gyro;
            accdot=cross(acc,gyro)';
            w3=ax*(-accdot(2)*az+accdot(3)*ay)/(ay^2+az^2);
            alphadiff=(inertgyro(3)-w3)*h;      % Equation 6
            
            if(abs(alphadiff)>alpha_eps)
                alpha=alpha+alphadiff;
            end
            ca=cos(alpha);
            sa=sin(alpha);
            Ra=[ca,-sa,0;
                sa,ca,0;
                0,0,1];
            R_alt=Ra*R_C*Rv_alt;
            R_C=R_alt*Rv';
            alpha=0;
            flag=1;
        end

        inertgyro=Rv*gyro;
        accdot=cross(acc,gyro)';
        w3=-(accdot(2)*acc(1)-accdot(1)*acc(2))/(az-1);

    %% Upper Hemisphere
    else
        % Compute R_E using Proposition III.1. 
        % If costhetap >= costhresh, then a transition occured. Hence, use 
        % the modified Rodriguez's formula to estimate Rv, alphadiff using 
        % Equation 6 and alpha by integration to obtain R using Rv,R_C and 
        % alpha. Conclude, by updating R_C using R, R_E and setting alpha=0. 
        % Else, update alpha using alphadiff (Eqn 6) and estimate R using 
        % R_E,alpha and R_C.
        
        ay2az2=sqrt(ay^2+az^2);
        Rv=[ay2az2,-(ax*ay)/ay2az2,-(ax*az)/ay2az2;
                0,-az/ay2az2,ay/ay2az2;
                -ax,-ay,-az];               % Proposition III.1
        if(costhetap>=costhresh)
            n=cross(acc,-e3);                                     
            K=[0,-n(3),n(2);
              n(3),0,-n(1);
              -n(2),n(1),0];   
            Rv_alt=eye(3,3)+K+(K*K)/(1+costheta);        
            inertgyro=Rv_alt*gyro;
            accdot=cross(acc,gyro)';
            w3=-(accdot(2)*ax-accdot(1)*ay)/(az-1);
            alphadiff=(inertgyro(3)-w3)*h;  % Equation 6
            if(abs(alphadiff)>alpha_eps)
                alpha=alpha+alphadiff;
            end
            ca=cos(alpha);
            sa=sin(alpha);
            Ra=[ca,-sa,0;
                sa,ca,0;
                0,0,1];
            R_alt=Ra*R_C*Rv_alt;
                
            R_C=R_alt*Rv';
            alpha=0;
            flag=1;
        end
        
        inertgyro=Rv*gyro;
        accdot=cross(acc,gyro)';
        w3=ax*(-accdot(2)*az+accdot(3)*ay)/(ay^2+az^2);
    end
    
    if(flag==1)
        R=R_alt*Rb;                                 % R_alt is R_{G}^{I_0}
    else
        alphadiff=(inertgyro(3)-w3)*h;              % Equation 6
        if(abs(alphadiff)>alpha_eps)
                alpha=alpha+alphadiff;
        end
        Rv=R_C*Rv*Rb;
        ca=cos(alpha);
        sa=sin(alpha);
        Ra=[ca,-sa,0;sa,ca,0;0,0,1];
        R=Ra*Rv;
    end
end