% Implementation of TRIAD algorithm
%
% Implementation by Abraham P Vinod (aby.vinod@gmail.com)
%
% Arguments
% =========
%
% mag               - Normalized magnetometer readout
% acc               - Normalized vector pointing to the body frame resolution of downward
%                      vertical (generalized to -e_3)
% tht_declination   - Magnetic declination (expressed in rads)
%
% Outputs
% =======
%
% RmatTRIAD         - Attitude estimate using TRIAD algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [RmatTRIAD]=TRIAD(mag,acc,tht_declination)
    % Rcorr is the correction for the magnetic declination
    Rcorr = [cos(tht_declination) -sin(tht_declination) 0; sin(tht_declination) cos(tht_declination) 0;0 0 1];
    % accelerometer is the body resolution of -e_3 vector
    r3 = acc;
    % Gram-schmidt orthogonalization
    m1b = mag-(r3.'*mag)*r3;
    r1 = m1b/norm(m1b);
    % Rows of rotation matrix are orthonormal
    r2 = cross(r3,r1);
    RmatTRIAD= Rcorr*[r1.'; r2.'; r3.'];
end