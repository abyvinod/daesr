% Wrapper code calling TRIAD.m, DAESR.m and noncomp.m to compute the
% position estimates and angular velocity estimates corresponding to TRIAD,
% DAESR,TRIAD+CF and DAESR+CF.
%
% Uses - TRIAD.m, DAESR.m, noncomp.m, getpos.m
%
% Implementation by Abraham P Vinod (aby.vinod@gmail.com)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dt = 17/819.2; % For 50 Hz, ADS16400 IMU gives a sampling rate of 48.1882 Hz.

rad = 0.145;   % Radius of the spherical robot

% Nonlinear complimentary filter tuning.
kmahoT=.05;
kmahoD=.1;
% Note that: the tuning factor is very small for TRIAD in order to get a
% reliable attitude estimate, but for the DAESR, because of being a robust
% estimate, the tuning factor can be pushed up higher. The tuning factor is
% a measure of the trust on the measured attitude estimate.

n2 = size(x,2);

% Fix the origin as the point where the robot starts.
xposT=0;
yposT=0;
xposD=0;
yposD=0;
xposTO=0;
yposTO=0;
xposDO=0;
yposDO=0;

% Initialization of vectors
xTRIAD=[];
yTRIAD=[];
xDAESR=[];
yDAESR=[];
xTRIAD_O=[];
yTRIAD_O=[];
xDAESR_O=[];
yDAESR_O=[];

alpha_eps=0.001;                    % Cutoff for integration to avoid noise                          
alpha=0;                            % Initial value of alpha
Ke3=[0,1,0;-1,0,0;0,0,0];
R_alpha_init=eye(3,3)+Ke3*sin(delta)+Ke3*Ke3*(1-cos(delta));

% Rz-correction for TRIAD from magnetic declination measured at the lab
% (chennai, India).
acc = [x(5,1); x(6,1); x(7,1)];
acc = acc/norm(acc);
mag = [x(8,1); x(9,1); x(1,1)];
mag = mag/norm(mag);
rcapTRIAD =TRIAD(mag,-acc,tht_declination);

gy = [x(2,1);x(3,1);x(4,1)];
accp=acc;
[rmatDAESR,alpha,R_alpha_init]= DAESR(acc,gy,dt,alpha,accp,eye(3,3),R_alpha_init,alpha_eps);
rcapDAESR = rmatDAESR;

for i=2:n2
    
    % Get the normalized readouts
    mag = [x(8,i); x(9,i); x(1,i)];
    mag = mag/norm(mag);
    acc = [x(5,i); x(6,i); x(7,i)];
    acc = acc/norm(acc);
    accp= [x(5,i-1); x(6,i-1); x(7,i-1)];
    accp= accp/norm(accp);
    
    gy = [x(2,i);x(3,i);x(4,i)];
    wcap = [0 -gy(3) gy(2); gy(3) 0 -gy(1); -gy(2) gy(1) 0];
    
    % Get TRIAD and position estimate and angular velocity
    rmatTRIAD=TRIAD(mag,-acc,tht_declination);
    [xposT,yposT]=getpos(xposT,yposT,rmatTRIAD,gy,rad,dt);
    xTRIAD = [xTRIAD xposT];
    yTRIAD = [yTRIAD yposT];

    % Get TRIAD+CF and position estimate and angular velocity
    rcapTRIAD=noncomp(rcapTRIAD,wcap,dt,kmahoT,rmatTRIAD);
    [xposTO,yposTO]=getpos(xposTO,yposTO,rcapTRIAD,gy,rad,dt);
    xTRIAD_O = [xTRIAD_O xposTO];
    yTRIAD_O = [yTRIAD_O yposTO];
    
    % Get DAESR and position estimate and angular velocity
    [rmatDAESR,alpha,R_alpha_init]=DAESR(acc,gy,dt,alpha,accp,eye(3,3),R_alpha_init,alpha_eps);
    [xposD,yposD]=getpos(xposD,yposD,rmatDAESR,gy,rad,dt);
    xDAESR = [xDAESR xposD];
    yDAESR = [yDAESR yposD];

    
    % Get DAESR+CF and position estimate and angular velocity
    rcapDAESR=noncomp(rcapDAESR,wcap,dt,kmahoD,rmatDAESR);
    [xposDO,yposDO]=getpos(xposDO,yposDO,rcapDAESR,gy,rad,dt);
    xDAESR_O = [xDAESR_O xposDO];
    yDAESR_O = [yDAESR_O yposDO];
end