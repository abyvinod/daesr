% Example code comparing the performance of TRIAD vs DAESR. Creates 2 plots
% for each of the 10 mat-files.
%
% Uses - wrapper_attitude.m
%
% Implementation by Abraham P Vinod (aby.vinod@gmail.com)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
clc

% If you don't want to plot 20 figures, set it to 0.
plot20fig=1;       
% Skip points to make the plots lesser cluttered
plot_skip=12;
% Setup the mat files
indx_array=1:10;
filename=['../data/tri1';'../data/tri2';'../data/tri3';'../data/tri4';'../data/tri5';'../data/cir1';'../data/cir2';'../data/cir3';'../data/cir4';'../data/cir5'];
% Problems arising from the symmetry of the spherical robot and human error
% The orientation along z-axis at t=0 => R_{I_0}^{I}=R_z(\alpha[0])=R_C[0]
delta_array=[pi,pi/2,pi,-pi/2,pi/2,pi,pi,pi,pi,-pi/2];
delta_declination_array=[zeros(1,5),pi,zeros(1,4)];
        
for indx=indx_array
    load(filename(indx,:));
    delta=delta_array(indx);
    tht_declination=delta_declination_array(indx)-0.2927;                  

    % Call the wrapper which compares TRIAD and DAESR's performance
    wrapper_attitude
    % gives xXXXXX and gyroXXXXX for each algorithm. XXXXX_O is the
    % complimentary filter outputs corresponding to the measured
    % attitude estimate XXXXX.

    % Sample the radial angle between zero to 2pi for the polar plots
    th_true=linspace(0,2*pi,n2);
    % Get the truth curves for trifolium and circle using polar
    % coordinates. The origin for the position estimation is at the point 
    % where the robot starts. Therefore, for trifolium, we need to shift 
    % the origin up by a petal's length. For the circle, we need to
    % move the origin to the left by the radius.
    if(indx<=5)
        % Trifolium
        % Magnetic declination measured at the lab (chennai, India).
        rtri=-2*cos(3*th_true-pi/2);
        y1=rtri.*cos(th_true);
        y2=rtri.*sin(th_true)-2;
    else
        % Circle
        % pi-declination because the spherical robot was started in the 
        % inverted position
        rtri=1.25;
        y1=rtri.*cos(th_true)+1.25;
        y2=rtri.*sin(th_true);    
    end

    %% Plotting: Straightfoward use of commands for aesthetics => No comments
    if plot20fig
        fig=figure(indx*2-1);
        set(fig, 'WindowStyle', 'docked');
        clf
        hold on;
        plot(y1,y2,'r--','linewidth',3);
        set(gca,'fontsize',20);
        plot(xTRIAD(1:plot_skip:end),yTRIAD(1:plot_skip:end),'mo','linewidth',3,'Markersize',10);
        plot(xDAESR(1:plot_skip:end),yDAESR(1:plot_skip:end),'gd','linewidth',3,'Markersize',10);
        plot(xTRIAD_O(1:plot_skip:end),yTRIAD_O(1:plot_skip:end),'bp','linewidth',3,'Markersize',10);
        plot(xDAESR_O(1:plot_skip:end),yDAESR_O(1:plot_skip:end),'k^','linewidth',3,'Markersize',10);
        leg=legend('True','TRIAD','DAESR','TRIAD+CF','DAESR+CF');
        set(leg,'fontsize',25);
        hold off;
        if(indx <= 5);
                view([0,90]);
                xlabel('X (m)','fontsize',20,'interpreter','latex');
                ylabel('Y (m)','fontsize',20,'interpreter','latex');
                % If aesthestics required, use this:
                ylim([-3.7 0.5])
                axis equal;
                box on;
                str=sprintf('Trifolium %d : Estimating positions from attitude estimates',indx);
        else
                set(gca,'fontsize',20);
                xlabel('X (m)','fontsize',20,'interpreter','latex');
                ylabel('Y (m)','fontsize',20,'interpreter','latex');
                box on;
                axis equal;
                str=sprintf('Circle %d : Estimating positions from attitude estimates',indx);
        end
        grid on;
        set(gca,'GridAlpha',0.5);
        set(gca,'fontsize',20);
        title(str,'interpreter','latex');
        set(leg,'Location','southoutside','Orientation','Horizontal');
    end

    %% Rotating the positions with mean aligned to lie on the line joining origin and the centroid'
    % Analysis to see how well is the shape maintained. This makes
    % sense because for a spherical robot's symmetry, the inertial
    % frame depends on the robot's first orientation and small
    % deviations brought in by human error can cause huge deviations.
    if indx<=5
        Ke3=[0,1,0;-1,0,0;0,0,0];
        sinTRIAD=mean(xTRIAD)/sqrt(mean(xTRIAD)^2+mean(yTRIAD)^2);
        cosTRIAD=-mean(yTRIAD)/sqrt(mean(xTRIAD)^2+mean(yTRIAD)^2);
        Rot=eye(3,3)+Ke3*sinTRIAD+Ke3*Ke3*(1-cosTRIAD);
        TRIAD_centered=Rot*[xTRIAD;yTRIAD;zeros(1,length(xTRIAD))];
        xTRIAD=TRIAD_centered(1,:);
        yTRIAD=TRIAD_centered(2,:);
        sinDAESR=mean(xDAESR)/sqrt(mean(xDAESR)^2+mean(yDAESR)^2);
        cosDAESR=-mean(yDAESR)/sqrt(mean(xDAESR)^2+mean(yDAESR)^2);
        Rot=eye(3,3)+Ke3*sinDAESR+Ke3*Ke3*(1-cosDAESR);
        DAESR_centered=Rot*[xDAESR;yDAESR;zeros(1,length(xDAESR))];
        xDAESR=DAESR_centered(1,:);
        yDAESR=DAESR_centered(2,:);
        sinTRIAD_O=mean(xTRIAD_O)/sqrt(mean(xTRIAD_O)^2+mean(yTRIAD_O)^2);
        cosTRIAD_O=-mean(yTRIAD_O)/sqrt(mean(xTRIAD_O)^2+mean(yTRIAD_O)^2);
        Rot=eye(3,3)+Ke3*sinTRIAD_O+Ke3*Ke3*(1-cosTRIAD_O);
        TRIAD_O_centered=Rot*[xTRIAD_O;yTRIAD_O;zeros(1,length(xTRIAD_O))];
        xTRIAD_O=TRIAD_O_centered(1,:);
        yTRIAD_O=TRIAD_O_centered(2,:);
        sinDAESR_O=mean(xDAESR_O)/sqrt(mean(xDAESR_O)^2+mean(yDAESR_O)^2);
        cosDAESR_O=-mean(yDAESR_O)/sqrt(mean(xDAESR_O)^2+mean(yDAESR_O)^2);
        Rot=eye(3,3)+Ke3*sinDAESR_O+Ke3*Ke3*(1-cosDAESR_O);
        DAESR_O_centered=Rot*[xDAESR_O;yDAESR_O;zeros(1,length(xDAESR_O))];
        xDAESR_O=DAESR_O_centered(1,:);
        yDAESR_O=DAESR_O_centered(2,:);
        str=sprintf('Trifolium %d : Rotated estimated positions with resp. means lying on the line joining $(0,0)$ to $(0,-2)$',indx);
    else
        Ke3=[0,1,0;-1,0,0;0,0,0];
        % R_z(-pi/2) to correct axes (now x is pointing up in the plot)
        RotCorr=eye(3,3)-Ke3+Ke3*Ke3;               
        sinTRIAD=mean(xTRIAD)/sqrt(mean(xTRIAD)^2+mean(yTRIAD)^2);
        cosTRIAD=-mean(yTRIAD)/sqrt(mean(xTRIAD)^2+mean(yTRIAD)^2);
        Rot=eye(3,3)+Ke3*sinTRIAD+Ke3*Ke3*(1-cosTRIAD);
        TRIAD_centered=RotCorr*Rot*[xTRIAD;yTRIAD;zeros(1,length(xTRIAD))];
        xTRIAD=TRIAD_centered(1,:);
        yTRIAD=TRIAD_centered(2,:);
        sinDAESR=mean(xDAESR)/sqrt(mean(xDAESR)^2+mean(yDAESR)^2);
        cosDAESR=-mean(yDAESR)/sqrt(mean(xDAESR)^2+mean(yDAESR)^2);
        Rot=eye(3,3)+Ke3*sinDAESR+Ke3*Ke3*(1-cosDAESR);
        DAESR_centered=RotCorr*Rot*[xDAESR;yDAESR;zeros(1,length(xDAESR))];
        xDAESR=DAESR_centered(1,:);
        yDAESR=DAESR_centered(2,:);
        sinTRIAD_O=mean(xTRIAD_O)/sqrt(mean(xTRIAD_O)^2+mean(yTRIAD_O)^2);
        cosTRIAD_O=-mean(yTRIAD_O)/sqrt(mean(xTRIAD_O)^2+mean(yTRIAD_O)^2);
        Rot=eye(3,3)+Ke3*sinTRIAD_O+Ke3*Ke3*(1-cosTRIAD_O);
        TRIAD_O_centered=RotCorr*Rot*[xTRIAD_O;yTRIAD_O;zeros(1,length(xTRIAD_O))];
        xTRIAD_O=TRIAD_O_centered(1,:);
        yTRIAD_O=TRIAD_O_centered(2,:);
        sinDAESR_O=mean(xDAESR_O)/sqrt(mean(xDAESR_O)^2+mean(yDAESR_O)^2);
        cosDAESR_O=-mean(yDAESR_O)/sqrt(mean(xDAESR_O)^2+mean(yDAESR_O)^2);
        Rot=eye(3,3)+Ke3*sinDAESR_O+Ke3*Ke3*(1-cosDAESR_O);
        DAESR_O_centered=RotCorr*Rot*[xDAESR_O;yDAESR_O;zeros(1,length(xDAESR_O))];
        xDAESR_O=DAESR_O_centered(1,:);
        yDAESR_O=DAESR_O_centered(2,:);
        str=sprintf('Circle %d : Rotated estimated positions with resp. means lying on the line joining $(0,0)$ to $(0,1.25)$',indx-5);
    end

    if plot20fig
        fig=figure(indx*2);
        set(fig, 'WindowStyle', 'docked');
        clf
        hold on;
        plot(y1,y2,'r--','linewidth',3);
        set(gca,'fontsize',20);
        plot(xTRIAD(1:plot_skip:end),yTRIAD(1:plot_skip:end),'mo','linewidth',3,'Markersize',10);
        plot(xDAESR(1:plot_skip:end),yDAESR(1:plot_skip:end),'gd','linewidth',3,'Markersize',10);
        plot(xTRIAD_O(1:plot_skip:end),yTRIAD_O(1:plot_skip:end),'bp','linewidth',3,'Markersize',10);
        plot(xDAESR_O(1:plot_skip:end),yDAESR_O(1:plot_skip:end),'k^','linewidth',3,'Markersize',10);
        leg=legend('True','TRIAD','DAESR','TRIAD+CF','DAESR+CF');
        set(leg,'fontsize',25);
        hold off;
        if(indx <= 5);
                view([0,90]);
                xlabel('X (m)','fontsize',20,'interpreter','latex');
                ylabel('Y (m)','fontsize',20,'interpreter','latex');
                % If aesthestics required, use this:
                ylim([-3.7 0.5])
                axis equal;
                box on;
        else
                set(gca,'fontsize',20);
                xlabel('X (m)','fontsize',20,'interpreter','latex');
                ylabel('Y (m)','fontsize',20,'interpreter','latex');
                box on;
                axis equal;
        end
        grid on;
        set(gca,'GridAlpha',0.5);
        set(gca,'fontsize',20);                        
        title(str,'interpreter','latex');
        set(leg,'Location','southoutside','Orientation','Horizontal');
    end
end
% The best case :)
if plot20fig
    figure(10);
end
